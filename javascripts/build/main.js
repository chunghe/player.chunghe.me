
/** @jsx React.DOM */


var PlayerControl = React.createClass({displayName: 'PlayerControl',

  onChange: function () {
    var player = this.props.player;
    var time = player.duration * (this.refs.range.getDOMNode().value / 100);
    player.currentTime = time;
  },

  onMouseDown: function () {
    this.props.player.pause();
  },

  onMouseUp: function () {
    this.props.player.play();
  },
  
  render: function () {
    return (
      React.DOM.div( {id:"controls"}, 
        React.DOM.nav(null, 
          React.DOM.span( {onClick:this.props.prev, className:"glyphicon glyphicon-backward"}),
          React.DOM.span( {onClick:this.props.play, className:"glyphicon glyphicon-play"}),
          React.DOM.span( {onClick:this.props.pause, className:"glyphicon glyphicon-pause"}),
          React.DOM.span( {onClick:this.props.next, className:"glyphicon glyphicon-forward"})
        ),
        React.DOM.input( {onMouseUp:this.onMouseUp, onMouseDown:this.onMouseDown, onChange:this.onChange, type:"range", ref:"range", value:this.props.range} )
      )
    );
  }
});

var PlayerList = React.createClass({displayName: 'PlayerList',

  isAcceptedType: function (file) {
    var acceptedTypes = {
      'audio/mp3': true, // chrome
      'audio/mpeg': true // firefox
    };
    return acceptedTypes[file.type]
  },

  onDragEnter: function (e) {
    return false;
  },

  onDragOver: function (e) {
    return false;
  },

  onDrop: function (e) {
    var items = e.nativeEvent.dataTransfer.items;
    var files = [].slice.call(e.nativeEvent.dataTransfer.files).filter(this.isAcceptedType);
    this.props.addFiles(files);
    return false;
  },

  render: function () {
    var self = this;
    var createItem = function (file, index) {
      return (
        React.DOM.li( {className:index === self.props.current ? 'playing': '', onClick:self.props.play.bind(this, index)}, 
          React.DOM.span( {className:"glyphicon glyphicon-music"}),file.name
        )
      );
    };

    return (
      React.DOM.ul( {id:"list", className:"hover", onDragEnter:this.onDragEnter, onDragOver:this.onDragOver, onDrop:this.onDrop}, 
        this.props.files.length ? this.props.files.map(createItem) : React.DOM.p( {className:"message"}, "Drop Files Here")
      )
    )
  }
})

var PlayerApp = React.createClass({displayName: 'PlayerApp',

  getInitialState: function () {
    return {files: [], range: 0, current: -1, playing: false}
  },

  componentWillUnmount: function () {
    this.player.removeEventListener('timeupdate', this.onTimeupdate);
    this.player.removeEventListener('ended', this.next);
    this.player = null;
  },

  componentDidMount: function () {
    this.player = new Audio();
    this.player.addEventListener('timeupdate', this.onTimeupdate);
    this.player.addEventListener('ended', this.next);
  },

  onTimeupdate: function (e) {
    var value = (100/this.player.duration) * this.player.currentTime;
    if (!isNaN(value)) {
      this.setState({range: value})
    }
  },

  prev: function () {
    this.play(this.state.current - 1);
  },

  next: function () {
    this.play(this.state.current + 1);
  },

  pause: function () {
    this.player.pause();
    document.title = this.state.files[this.state.current].name;
    this.setState({playing: false});
  },

  addFiles: function (files) {
    this.setState({files: this.state.files.concat(files)});
  },

  play: function (index) {
    var indexNotANumber = isNaN(index); // click play icon in the controls will not pass index
    if (indexNotANumber && this.state.current !== -1) {
      this.player.play();
      this.setState({playing: true})
      return false;
    }

    if (indexNotANumber) {
      index = 0;
    }

    if (this.state.files[index]) {
      var file = this.state.files[index];
      var src = window.URL.createObjectURL(file);
      document.title = '♫  ' + this.state.files[index].name;
      this.player.src = src;
      this.player.play();
      this.setState({current: index, playing: true});
    }
  },

  render: function () {
    return (
      React.DOM.div( {id:"player", ref:"player", className:this.state.playing? 'playing': ''}, 
        PlayerControl( {range:this.state.range, player:this.player, next:this.next, prev:this.prev, pause:this.pause, play:this.play}  ),
        PlayerList( {files:this.state.files, addFiles:this.addFiles, play:this.play, current:this.state.current} )
      )
    );
  }
});

React.renderComponent(PlayerApp(null ), document.body);
